#ifndef SAND_H_
#define SAND_H_

#include "bluepill.h"




class Sand {
private:
	u8	rows;
	u8	columns;


public:

	u8** shape;

			Sand(u8 r, u8 c);
			~Sand();
	void	shape_clear(void);
	void	shape_init(u8 part = 1);
	void	pick_grain(void);
	void	shape_gravity_impact(void);

};

#endif /* SAND_H_ */
