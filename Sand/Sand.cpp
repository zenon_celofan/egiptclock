#include "Sand.h"
#include <stdlib.h>
#include "bluepill.h"

Sand::Sand(u8 r, u8 c) {

	rows = r;
	columns = c;

	shape = (u8**) malloc(rows*sizeof(u32));

	for (u8 r = 0; r < rows; r++) {
		shape[r] = (u8*) malloc(columns);
	}

} //Sand()



Sand::~Sand(void) {
	for (u8 r = 0; r < rows; r++) {
		free(shape[r]);
	}

	free(shape);
} //~Sand()



void Sand::shape_init(u8 part) {
	for (u8 r = 0; r < rows; r++) {
		for (u8 c = 0; c < columns; c++) {
			if ((c >= part * 8) && (c < (part * 8 + 8)) ) {
				shape[r][c] = 1;
			} else {
				shape[r][c] = 0;
			}
		}
	}
} //shape_init()



void Sand::pick_grain(void) {
	u8 sum = 0;
	u8 random_number;

	for (u8 c = columns-1; c >= columns-8; c--) {

		for (u8 i = 0; i < rows; i++) {
			if (shape[i][c] != 0)
				sum++;
		}

		if (sum == 0)
			continue;

		random_number = random(sum) + 1;

		for (u8 r = 0; r < rows; r++) {
			if (shape[r][c] != 0) {
				if (random_number < sum) {
					random_number++;
					continue;
				} else {
					shape[r][c] = 0;
				}


				//put grain in empty space
				while (1) {
					u8 point = random(8);
					if (shape[point][columns - 8 - 1] == 0) {
						shape[point][columns - 8 - 1] = 1;
						return;
					}
				}
			}
		}
	}
} //pick_grain()



void Sand::shape_clear() {
	for (u8 r = 0; r < rows; r++) {
		for (u8 c = 0; c < columns; c++) {
				shape[r][c] = 0;
		}
	}
} //shape_clear()



void Sand::shape_gravity_impact(void) {
	s8	q = random(2);
	if (q==0) q = -1;

	for (u8 c = 1; c < columns - 8; c++) {
		for (u8 r = 0; r < rows; r++) {
			if (shape[r][c] != 0) {
				if (shape[r][c-1] == 0) {
					shape[r][c-1] = 1;
					shape[r][c] = 0;
				} else if ((r+q>=0) && (r+q<=rows-1) && (shape[r+q][c-1] == 0)) {
					shape[r+q][c-1] = 1;
					shape[r][c] = 0;
				} else if ((r-q>=0) && (r-q<=rows-1) && (shape[r-q][c-1] == 0)) {
					shape[r-q][c-1] = 1;
					shape[r][c] = 0;
				}
			}
		}
	}
} //shape_gravity_impact()
