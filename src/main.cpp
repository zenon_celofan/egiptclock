#include <MatrixDisplay.h>
#include <stdio.h>
#include <stdlib.h>
#include "bluepill.h"
#include "Photoresistor.h"
#include "Pin.h"
#include "Led.h"
#include "delay.h"
#include "Sand.h"

#define	GRAIN_FALL_INTERVAL		60000



int main() {

	millis_init();

	Led* led13 = new Led;
	led13->turn_on();

	Pin* hg_switch = new Pin(PB13, GPIO_Mode_IPU);
	Pin* hg_switch_gnd = new Pin(PB12);
	hg_switch_gnd->digital_write(Bit_RESET);   //virtual GND for hg_sensor

	hg_switch_gnd->digital_write(Bit_RESET);   //virtual GND for hg_sensor
	u8 direction = 0;

	Photoresistor* light_sensor = new Photoresistor(PA2, ADC2, ADC_Channel_2);

	delay(100);  //Let them MAX7219 boot up

	Matrix_Display*	led_matrix = new Matrix_Display(PB7, PB8, PB9, 2);
	led_matrix->reset();
	led_matrix->set_brightness(0);
	led_matrix->clear_display();

	Sand* sand = new Sand(8, 16);

	led13->turn_off();

	u32 grain_time = millis();

	sand->shape_init();

	while(1) {
		led13->set(hg_switch->digital_read());

		if (hg_switch->digital_read() != direction) {
			sand->shape_init();
			direction = !direction;
		}

		if (direction != 0)
			led_matrix->display((u8**) sand->shape);
		else
			led_matrix->display_upsidedown((u8**) sand->shape);

		sand->shape_gravity_impact();
		delay(100);

		if (millis() - grain_time > GRAIN_FALL_INTERVAL) {
			sand->pick_grain();
			grain_time = millis();
		}
	}
} //main()
