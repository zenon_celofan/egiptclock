#ifndef PHOTORESISTOR_H_
#define PHOTORESISTOR_H_

#include "bluepill.h"
#include "stm32f10x_adc.h"
#include "Pin.h"



class Photoresistor {

public:
	Photoresistor(uint16_t photo_pin, ADC_TypeDef* adcx, u8 channelx);
	u16	get_sensor_value(void);

private:
	class Pin	sensor_signal_pin;

	ADC_TypeDef*	adc_number;
	u8				adc_channel;

	u16				sensor_reading;

	void init_adc(void);

};

#endif /* PHOTORESISTOR_H_ */
