#include "Photoresistor.h"
#include "bluepill.h"
#include "stm32f10x_gpio.h"
#include "Pin.h"



Photoresistor::Photoresistor(uint16_t photo_pin, ADC_TypeDef* adcx, u8 channelx) : sensor_signal_pin(photo_pin, GPIO_Mode_IN_FLOATING, GPIO_Speed_2MHz, Bit_RESET){

	adc_number = adcx;
	adc_channel = channelx;

	sensor_reading = 0;

	init_adc();
} //Photoresistor()



void Photoresistor::init_adc(void) {

    //ADC init
	if (adc_number == ADC1) {
		RCC_APB2PeriphClockCmd(RCC_APB2Periph_ADC1, ENABLE);
	}
	else if (adc_number == ADC2) {
		RCC_APB2PeriphClockCmd(RCC_APB2Periph_ADC2, ENABLE);
	}
	else if (adc_number == ADC3) {
		RCC_APB2PeriphClockCmd(RCC_APB2Periph_ADC3, ENABLE);
	}

	ADC_InitTypeDef ADC_InitStructure;

	RCC_ADCCLKConfig(RCC_PCLK2_Div6);

	ADC_DeInit(adc_number);
	ADC_InitStructure.ADC_Mode = ADC_Mode_Independent;
	ADC_InitStructure.ADC_ScanConvMode = DISABLE;
	ADC_InitStructure.ADC_ContinuousConvMode = ENABLE;
	ADC_InitStructure.ADC_ExternalTrigConv = ADC_ExternalTrigConv_None;
	ADC_InitStructure.ADC_DataAlign = ADC_DataAlign_Right;
	ADC_InitStructure.ADC_NbrOfChannel = 1;
	ADC_Init(adc_number, &ADC_InitStructure);

	ADC_Cmd(adc_number, ENABLE);

	ADC_ResetCalibration(adc_number);
	while (ADC_GetResetCalibrationStatus(adc_number));

	ADC_StartCalibration(adc_number);
	while (ADC_GetCalibrationStatus(adc_number));

	ADC_RegularChannelConfig(adc_number, adc_channel, 1, ADC_SampleTime_239Cycles5);

	ADC_SoftwareStartConvCmd(adc_number, ENABLE);
} //init_pin()



u16	Photoresistor::get_sensor_value(void) {
	return ADC_GetConversionValue(adc_number);
} //get_value()
