#include "MatrixDisplay.h"
#include "bluepill.h"
#include <stdlib.h>



Matrix_Display::Matrix_Display(u16 matrix_din_pin, u16 matrix_cs_pin , u16 matrix_clk_pin, u8 number_of_matrixes) :
  matrix_din_pin(matrix_din_pin, GPIO_Mode_Out_OD),
  matrix_cs_pin(matrix_cs_pin, GPIO_Mode_Out_OD),
  matrix_clk_pin(matrix_clk_pin, GPIO_Mode_Out_OD) {

	number_of_led_modules = number_of_matrixes;
	rows = 8;
	columns = 8 * number_of_led_modules;
	reset();
} //Matrix_Display()



void Matrix_Display::set_brightness(u8 value) {
	if (value > 0x0F)
		value = 0x0F;

	send_command_to_all_modules(0x0A, value);
} //set_brightness()



void Matrix_Display::push_16_bits(u16 packet) {
	for (s8 i = 15; i >= 0; i--) {
		if ((packet >> i & 1) != 0) {
			matrix_din_pin.digital_write(Bit_SET);
		} else {
			matrix_din_pin.digital_write(Bit_RESET);
		}
		matrix_clk_pin.digital_write(Bit_RESET);
		matrix_clk_pin.digital_write(Bit_SET);

	}
} //push_16_bits()



void	Matrix_Display::send_command_to_all_modules(u8 addr, u8 data) {

	u16 frame = (addr << 8) + data;

	matrix_cs_pin.digital_write(Bit_SET);
	matrix_din_pin.digital_write(Bit_RESET);
	matrix_clk_pin.digital_write(Bit_RESET);
	matrix_cs_pin.digital_write(Bit_RESET);

	for (u8 module = 0; module < number_of_led_modules; module++) {
		push_16_bits(frame);
	}

	matrix_cs_pin.digital_write(Bit_SET);

} //send_command



void	Matrix_Display::send_line_data(u8 line_number, u8* pixels) {
	line_number &= 0b00001111;

	matrix_cs_pin.digital_write(Bit_SET);
	matrix_din_pin.digital_write(Bit_RESET);
	matrix_clk_pin.digital_write(Bit_RESET);
	matrix_cs_pin.digital_write(Bit_RESET);

	for (u8 module = 0; module < number_of_led_modules; module++) {
		u8 data = 0;
		for (u8 i = 0; i < 8; i++) {
			data = (data << 1) + (*(pixels+module*8+i)>0?1:0);
		}
		u16 frame = (line_number << 8) + data;

		push_16_bits(frame);
	}

	matrix_cs_pin.digital_write(Bit_SET);

} //send_data()



void	Matrix_Display::display(u8** array) {
	if (rows != 8)
		return;
	if (columns % 8 != 0)
		return;

	for (u8 line = 1; line <= 8; line++) {
		send_line_data(line, array[line-1]);
	}

} //display()



void	Matrix_Display::display_upsidedown(u8** array) {
	u8**	rarray;

	if (rows != 8)
		return;
	if (columns % 8 != 0)
		return;

	//new reversed array
	rarray = (u8**) malloc(rows*sizeof(u32));
	for (u8 r = 0; r < rows; r++) {
		rarray[r] = (u8*) malloc(columns);
	}

	for (u8 r = 0; r < rows; r++) {
		for (u8 c = 0; c < columns; c++) {
			rarray[r][c] = array[rows-1-r][columns-1-c];
		}
	}

	//display array
	for (u8 line = 1; line <= rows; line++) {
		send_line_data(line, rarray[line-1]);
	}

	for (u8 r = 0; r < rows; r++) {
		free(rarray[r]);
	}
	free(rarray);

} //display_upsidedown()



void Matrix_Display::shutdown(bool set_shutdown) {
	if (set_shutdown == true)
		send_command_to_all_modules(0x0C, 0x00);
	else
		send_command_to_all_modules(0x0C, 0x01);
} //shutdown()



void  Matrix_Display::test_mode(bool set_test_mode) {
	if (set_test_mode == true)
		send_command_to_all_modules(0x0F, 0x01);
	else
		send_command_to_all_modules(0x0F, 0x00);
} //display_test()



void Matrix_Display::clear_display(void) {
	for (u8 line = 1; line <= 8; line++) {
		send_command_to_all_modules(line, 0x00);
	}
} //clear_display()



void Matrix_Display::scan_limit(u8 max) {
	send_command_to_all_modules(0x0B, max);
} //scan_limit()



void Matrix_Display::reset(void) {
	shutdown(true);
	scan_limit();
	clear_display();
	set_brightness(0);
	shutdown(false);
} //reset()
