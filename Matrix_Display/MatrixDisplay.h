#ifndef MATRIXDISPLAY_H_
#define MATRIXDISPLAY_H_

#include "bluepill.h"
#include "stm32f10x_gpio.h"
#include "Pin.h"



class Matrix_Display {
private:
	class Pin	matrix_din_pin;
	class Pin	matrix_cs_pin;
	class Pin	matrix_clk_pin;

	u8	number_of_led_modules;
	u8	rows;
	u8  columns;

	void	push_16_bits(u16 packet);
	void	send_line_data(u8 line_number, u8* pixels);
	void	send_command_to_all_modules(u8 addr, u8 data);


public:

	Matrix_Display(u16 matrix_din_pin = PB7, u16 matrix_cs_pin = PB8, u16 matrix_clk_pin = PB9, u8 number_of_matrixes = 1);

	void	shutdown(bool set_shutdown = true);
	void	set_brightness(u8 value = 0);
	void	display(u8** array);
	void	display_upsidedown(u8** array);
	void	test_mode(bool set_test_mode = true);
	void	scan_limit(u8 max = 0x07);
	void	clear_display(void);
	void 	reset(void);


};

#endif // MATRIXDISPLAY_H_
